use crate::bencode::*;
use std::collections::HashMap;
use std::io::Cursor;

#[test]
fn decodes_int_value() {
    let data = b"i42e";
    let x = Value::decode(Cursor::new(data)).unwrap();
    match x {
        Value::Int(x) => assert_eq!(x, 42),
        _ => panic!("Invalid value type"),
    }
}

#[test]
#[should_panic]
fn fails_to_decode_unterminated_int() {
    let data = b"i42";
    let _ = Value::decode(Cursor::new(data)).unwrap();
}

#[test]
#[should_panic]
fn fails_to_decode_non_started_int() {
    let data = b"42e";
    let _ = Value::decode(Cursor::new(data)).unwrap();
}

#[test]
#[should_panic]
fn fails_to_decode_invalid_int() {
    let data = b"i4x2e";
    let _ = Value::decode(Cursor::new(data)).unwrap();
}

#[test]
fn decodes_str_value() {
    let data = b"6:foobar";
    let x = Value::decode(Cursor::new(data)).unwrap();
    match x {
        Value::Str(x) => assert_eq!(x, b"foobar"),
        _ => panic!("Invalid value type"),
    }
}

#[test]
#[should_panic]
fn fails_to_decode_too_short_string() {
    let data = b"42:abcde";
    let _ = Value::decode(Cursor::new(data)).unwrap();
}

#[test]
fn decodes_list_value() {
    let data = b"li1ei2ei3ee";
    let x = Value::decode(Cursor::new(data)).unwrap();
    match x {
        Value::List(x) => {
            assert_eq!(x[0], Value::Int(1));
            assert_eq!(x[1], Value::Int(2));
            assert_eq!(x[2], Value::Int(3));
        }
        _ => panic!("Invalid value type"),
    }
}

#[test]
fn decodes_dict_value() {
    let data = b"d6:foobari42e3:baz4:spame";
    let x = Value::decode(Cursor::new(data)).unwrap();
    match x {
        Value::Dict(x) => {
            match x.get("foobar").unwrap() {
                Value::Int(x) => assert_eq!(*x, 42),
                _ => panic!("Invalid value type"),
            };

            match x.get("baz").unwrap() {
                Value::Str(ref x) => assert_eq!(*x, b"spam"),
                _ => panic!("Invalid value type"),
            };
        }
        _ => panic!("Invalid value type"),
    }
}

#[test]
fn encodes_int_value() {
    let x = Value::Int(42i64).encode();
    assert_eq!(x, b"i42e");
}

#[test]
fn encodes_str_value() {
    let x = Value::Str(b"foobar".to_vec()).encode();
    assert_eq!(x, b"6:foobar");
}

#[test]
fn encodes_list_value() {
    use self::Value::*;

    let x = List(vec![Int(42i64), Str(b"foobar".to_vec())]).encode();
    assert_eq!(x, b"li42e6:foobare");
}

#[test]
fn encodes_dict_value() {
    let x = {
        let mut x = HashMap::new();
        x.insert("foo".into(), Value::Int(42i64));
        x.insert("bar".into(), Value::Int(13i64));
        x
    };

    let x = Value::Dict(x).encode();

    let x = String::from_utf8(x.clone()).unwrap();

    assert!(x.find("3:bari13e").is_some());
    assert!(x.find("3:fooi42e").is_some());
}
