#[test]
fn parses_compact_tracker_response() {
    use crate::bencode::*;
    use crate::tracker::*;
    use std::io::Cursor;

    let data = include_bytes!("data/tracker_compact_response.dat");
    let value = Value::decode(Cursor::new(&data[..])).unwrap();
    let response = parse_tracker_response(value).unwrap();
    let response = if let TrackerResponse::Success(r) = response {
        r
    } else {
        panic!("Invalid tracker response")
    };
    let peers = if let TrackerResponsePeerList::Compact(p) = response.peers {
        p
    } else {
        panic!("Unexpected peer list type")
    };
    assert_eq!(peers.len(), 50);
    for p in peers {
        println!("{}", p.addr);
    }
}
