use crate::bencode::*;
use std::io::Cursor;

#[test]
fn reads_single_string_token() {
    let data = b"7:13hello";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let token = tokenizer.next_token().unwrap();
    assert_eq!(token, Token::String(b"13hello".to_vec()));
}

#[test]
fn reads_single_int_token() {
    let data = b"i42e";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let token = tokenizer.next_token().unwrap();
    assert_eq!(token, Token::Int(42));
}

#[test]
fn reads_single_negative_int_token() {
    let data = b"i-42e";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let token = tokenizer.next_token().unwrap();
    assert_eq!(token, Token::Int(-42));
}

#[test]
fn reads_empty_int_token() {
    let data = "ie";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let token = tokenizer.next_token().unwrap();
    assert_eq!(token, Token::Int(0));
}

#[test]
fn fails_to_read_bad_int_token() {
    let data = b"i4foo2e";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token();
    assert!(x.is_err());
}

#[test]
fn reads_empty_list_value() {
    let data = "le";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ListStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);
}

#[test]
fn reads_simple_list_value() {
    let data = b"li42e3:fooe";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ListStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::Int(42));

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::String(b"foo".to_vec()));

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);
}

#[test]
fn reads_nested_list_value() {
    let data = b"lli42eee";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ListStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ListStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::Int(42));

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);
}

#[test]
fn reads_empty_dict_value() {
    let data = b"de";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::DictStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);
}

#[test]
fn reads_simple_dict_value() {
    let data = b"d3:fooi42ee";
    let stream = Cursor::new(data);
    let mut tokenizer = StreamTokenizer::new(stream);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::DictStart);

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::String(b"foo".to_vec()));

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::Int(42));

    let x = tokenizer.next_token().unwrap();
    assert_eq!(x, Token::ValueEnd);
}
