use crate::bencode::*;
use crate::torrent_info::*;
use std::convert::TryFrom;
use std::io::Cursor;

#[test]
fn decodes_single_file_torrent() {
    // Torrent contains standard "Lorem Ipsum" text
    let data = include_bytes!("data/lipsum.txt.torrent");
    let value = decode_value(Cursor::new(&data[..])).unwrap();
    let info = TorrentInfo::try_from(value).unwrap();

    assert_eq!(None, info.announce);
    assert_eq!(None, info.files);
    assert_eq!("lipsum.txt", info.name);
    assert_eq!(65536, info.piece_length);
    assert_eq!(1, info.pieces.len());

    // This hash is just plain copied from torrent file
    const DATA_HASH: Hash = [
        0xcd, 0x36, 0xb3, 0x70, 0x75, 0x8a, 0x25, 0x9b, 0x34, 0x84, 0x50, 0x84, 0xa6, 0xcc, 0x38,
        0x47, 0x3c, 0xb9, 0x5e, 0x27,
    ];

    let hash = info.pieces.get(0).unwrap();
    assert_eq!(*hash, DATA_HASH);
}

#[test]
fn decodes_multi_file_torrent() {
    let data = include_bytes!("data/adb_fastboot.torrent");
    let value = Value::decode(Cursor::new(&data[..])).unwrap();
    let info = TorrentInfo::try_from(value).unwrap();

    let tracker_url = info.announce.unwrap();
    assert_eq!(tracker_url, "http://tracker.openbittorrent.com:80/announce");

    let mut files = info.files.unwrap();
    assert_eq!(files.len(), 2);

    let f = files.remove(0);
    assert_eq!(f.path, vec!["adb.exe"]);
    assert_eq!(f.length, 1_656_320);

    let f = files.remove(0);
    assert_eq!(f.path, vec!["fastboot.exe"]);
    assert_eq!(f.length, 851_456);
}

#[test]
fn decodes_multi_file_torrent_with_folders() {
    let data = include_bytes!("data/adb_fastboot_in_folders.torrent");
    let value = Value::decode(Cursor::new(&data[..])).unwrap();
    let info = TorrentInfo::try_from(value).unwrap();

    let tracker_url = info.announce.unwrap();
    assert_eq!(tracker_url, "http://tracker.openbittorrent.com:80/announce");

    let mut files = info.files.unwrap();
    assert_eq!(files.len(), 2);

    let f = files.remove(0);
    assert_eq!(f.path, vec!["adb", "adb.exe"]);
    assert_eq!(f.length, 1_656_320);

    let f = files.remove(0);
    assert_eq!(f.path, vec!["fastboot", "fastboot.exe"]);
    assert_eq!(f.length, 851_456);
}
