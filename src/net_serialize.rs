#![allow(unused_macros)]
use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
use std::io::{Read, Result as IoResult, Write};

pub trait NetWrite: Sized {
    fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()>;
}

macro_rules! impl_net_write {
    ($t:ty, $f:ident) => {
        impl NetWrite for $t {
            fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
                w.$f::<NetworkEndian>(*self)?;
                Ok(())
            }
        }
    };
}

impl_net_write!(i16, write_i16);
impl_net_write!(u16, write_u16);
impl_net_write!(i32, write_i32);
impl_net_write!(u32, write_u32);
impl_net_write!(i64, write_i64);
impl_net_write!(u64, write_u64);

impl NetWrite for i8 {
    fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
        w.write_all(&[*self as u8])?;
        Ok(())
    }
}

impl NetWrite for u8 {
    fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
        w.write_all(&[*self])?;
        Ok(())
    }
}

pub trait NetRead: Sized {
    fn read_from<R: Read>(r: &mut R) -> IoResult<Self>;
}

macro_rules! impl_net_read {
    ($t:ty, $f:ident) => {
        impl NetRead for $t {
            fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
                r.$f::<NetworkEndian>()
            }
        }
    };
}

impl_net_read!(i16, read_i16);
impl_net_read!(u16, read_u16);
impl_net_read!(i32, read_i32);
impl_net_read!(u32, read_u32);
impl_net_read!(i64, read_i64);
impl_net_read!(u64, read_u64);

impl NetRead for i8 {
    fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
        let mut result = [0u8; 1];
        r.read_exact(&mut result[..])?;
        Ok(result[0] as i8)
    }
}

impl NetRead for u8 {
    fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
        let mut result = [0u8; 1];
        r.read_exact(&mut result[..])?;
        Ok(result[0])
    }
}

pub trait NetSerialize: NetRead + NetWrite {}

impl<T: NetRead + NetWrite> NetSerialize for T {}

#[doc(hidden)]
#[macro_export]
macro_rules! __impl_net_read_for_struct {
    ($name:ident {$($f:ident,)*}) => {
        impl NetRead for $name {
            fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
                Ok(Self{
                    $($f: NetRead::read_from(r)?,)*
                })
            }
        }
    };
}

#[macro_export]
macro_rules! net_read_struct {
    ($name:ident {
        $($f:ident : $t:ty,)*
    }) => {
        struct $name {
            $(pub $f: $t),*
        }

        $crate::__impl_net_read_for_struct!{$name { $($f,)* }}
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! __impl_net_write_for_struct {
    ($name:ident {$($f:ident,)*}) => {
        impl NetWrite for $name {
            fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
                $( NetWrite::write_to(&self.$f, w)?; )*
                Ok(())
            }
        }
    };
}

#[macro_export]
macro_rules! net_write_struct {
    ($name:ident {
        $($f:ident : $t:ty,)*
    }) => {
        struct $name {
            $(pub $f: $t,)*
        }

        $crate::__impl_net_write_for_struct!{$name { $($f,)* }}
    };
}

#[macro_export]
macro_rules! net_write_ref_struct {
    ($name:ident {
        $($f:ident : &$t:ty,)*
    }) => {
        struct $name<'a> {
            $(pub $f: &'a $t,)*
        }

        impl<'a> NetWrite for $name<'a> {
            fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
                $( NetWrite::write_to(self.$f, w)?; )*
                Ok(())
            }
        }
    };
}

#[macro_export]
macro_rules! net_serialize_struct {
    ($name:ident {
        $($f:ident : $t:ty,)*
    }) => {
        struct $name {
            $(pub $f: $t,)*
        }

        $crate::__impl_net_read_for_struct!{$name { $($f,)* }}
        $crate::__impl_net_write_for_struct!{$name { $($f,)* }}
    };
}
