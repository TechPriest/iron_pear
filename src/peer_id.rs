use crate::net_serialize::{NetRead, NetWrite};
use std::io::{Read, Result as IoResult, Write};

const PEER_ID_LENGTH: usize = 20;

#[derive(Debug, Clone)]
pub struct PeerId {
    data: [u8; PEER_ID_LENGTH],
}

impl PeerId {
    pub fn generate() -> Self {
        use rand::Rng;

        let chars = b"abcdefghijklmnopqrstuvwxyz\
            ABCDEFGHIJKLMNOPQRSTUVWXYZ\
            0123456789\
            -_.~";

        let mut rng = rand::thread_rng();
        let mut data = [0u8; 20];
        for c in &mut data {
            let ndx = rng.gen_range(0usize .. chars.len());
            *c = chars[ndx];
        }

        Self { data }
    }

    pub fn load(data: &[u8]) -> Self {
        let iter = data
            .iter()
            .cloned()
            .chain(std::iter::repeat(0u8))
            .take(PEER_ID_LENGTH);
        let mut tmp = [0u8; PEER_ID_LENGTH];
        for (k, v) in iter.enumerate() {
            tmp[k] = v;
        }
        Self { data: tmp }
    }

    pub fn data(&self) -> &[u8] {
        &self.data[..]
    }
}

impl AsRef<str> for PeerId {
    fn as_ref(&self) -> &str {
        use std::str;

        unsafe { str::from_utf8_unchecked(&self.data) }
    }
}

impl NetWrite for PeerId {
    fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
        w.write_all(&self.data)
    }
}

impl NetRead for PeerId {
    fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
        let mut data = [0u8; PEER_ID_LENGTH];
        r.read_exact(&mut data)?;
        Ok(PeerId { data })
    }
}
