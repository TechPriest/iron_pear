use sha1::{Digest, Sha1};
use std::iter::Iterator;

pub struct HashingIterWrapper<'a, T>
where
    T: Iterator<Item = u8>,
{
    iterator: &'a mut T,
    hash: Sha1,
    pub hashing_enabled: bool,
}

impl<'a, T> HashingIterWrapper<'a, T>
where
    T: Iterator<Item = u8>,
{
    pub fn new(iterator: &'a mut T) -> Self {
        Self {
            iterator,
            hash: Sha1::new(),
            hashing_enabled: false,
        }
    }

    pub fn finish(self) -> ([u8; 20], &'a T) {
        let mut result: [u8; 20] = std::default::Default::default();
        for (ndx, b) in self.hash.finalize().iter().take(20).enumerate() {
            result[ndx] = *b;
        }
        (result, self.iterator)
    }
}

impl<'a, T> Iterator for HashingIterWrapper<'a, T>
where
    T: Iterator<Item = u8>,
{
    type Item = u8;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        match self.iterator.next() {
            Some(x) => {
                if self.hashing_enabled {
                    self.hash.update(&[x]);
                }
                Some(x)
            }
            None => None,
        }
    }
}
