use crate::bencode::{DecodingError, Value};
use std;
use std::collections::HashMap;
use std::convert::{Into, TryFrom};

#[derive(Debug, Clone)]
pub enum ValueType {
    Int,
    Str,
    List,
    Dict,
}

#[derive(Debug, Clone)]
pub struct InvalidValueTypeError {
    pub key: &'static str,
    pub actual_type: ValueType,
    pub expected_type: ValueType,
}

impl InvalidValueTypeError {
    pub fn new(key: &'static str, actual_type: ValueType, expected_type: ValueType) -> Self {
        Self {
            key,
            actual_type,
            expected_type,
        }
    }
}

#[derive(Debug)]
pub enum ParseError {
    MissingKey(&'static str),
    InvalidValueType(InvalidValueTypeError),
    InvalidTextData,
    InvalidPiecesData,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FileDescription {
    pub length: i64,

    /// Path elements, "foo/bar/baz" = vec!["foo", "bar", "baz"]
    pub path: Vec<String>,
}

const KEY_LENGTH: &str = "length";
const KEY_PATH: &str = "path";

impl Into<Value> for FileDescription {
    fn into(self) -> Value {
        let tmp = {
            let mut tmp = std::collections::HashMap::new();
            tmp.insert(KEY_LENGTH.into(), Value::Int(self.length));
            tmp.insert(
                KEY_PATH.into(),
                Value::List(
                    self.path
                        .into_iter()
                        .map(|p| Value::Str(p.into_bytes()))
                        .collect(),
                ),
            );
            tmp
        };

        Value::Dict(tmp)
    }
}

type Dictionary = HashMap<String, Value>;
type ParseResult<T> = Result<T, ParseError>;

fn get_value_type(v: &Value) -> ValueType {
    match *v {
        Value::Int(_) => ValueType::Int,
        Value::Str(_) => ValueType::Str,
        Value::List(_) => ValueType::List,
        Value::Dict(_) => ValueType::Dict,
    }
}

fn parse_int_field(v: &Dictionary, k: &'static str) -> ParseResult<i64> {
    let v = v.get(k).ok_or_else(|| ParseError::MissingKey(k))?;

    if let Value::Int(x) = *v {
        Ok(x)
    } else {
        let e = InvalidValueTypeError::new(k, get_value_type(v), ValueType::Int);
        Err(ParseError::InvalidValueType(e))
    }
}

fn parse_str_field(v: &Dictionary, k: &'static str) -> ParseResult<Vec<u8>> {
    let make_fail_result = |actual_type: ValueType| -> ParseResult<_> {
        let e = InvalidValueTypeError::new(k, actual_type, ValueType::Str);
        Err(ParseError::InvalidValueType(e))
    };

    match *(v.get(k).ok_or_else(|| ParseError::MissingKey(k))?) {
        Value::Str(ref x) => Ok(x.clone()),
        Value::Int(_) => make_fail_result(ValueType::Int),
        Value::List(_) => make_fail_result(ValueType::List),
        Value::Dict(_) => make_fail_result(ValueType::Dict),
    }
}

fn parse_text_field(v: &Dictionary, k: &'static str) -> ParseResult<String> {
    let data = parse_str_field(v, k)?;
    Ok(String::from_utf8(data).or(Err(ParseError::InvalidTextData)))?
}

impl TryFrom<Value> for FileDescription {
    type Error = ParseError;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        if let Value::Dict(mut value) = value {
            let length = parse_int_field(&value, KEY_LENGTH)?;

            let paths = value
                .remove(KEY_PATH)
                .ok_or(ParseError::MissingKey(KEY_PATH))?;
            let paths = if let Value::List(paths) = paths {
                Ok(paths)
            } else {
                let e =
                    InvalidValueTypeError::new(KEY_PATH, get_value_type(&paths), ValueType::List);
                Err(ParseError::InvalidValueType(e))
            }?;

            let path: ParseResult<Vec<String>> = paths
                .into_iter()
                .map(|p| {
                    if let Value::Str(p) = p {
                        Ok(String::from_utf8(p).or(Err(ParseError::InvalidTextData))?)
                    } else {
                        let e = InvalidValueTypeError::new(
                            KEY_PATH,
                            get_value_type(&p),
                            ValueType::Str,
                        );
                        Err(ParseError::InvalidValueType(e))
                    }
                })
                .collect();
            let path = path?;
            Ok(Self { length, path })
        } else {
            let e = InvalidValueTypeError::new(KEY_FILES, get_value_type(&value), ValueType::Dict);
            Err(ParseError::InvalidValueType(e))
        }
    }
}

pub type Hash = [u8; 20];

#[derive(Debug, Clone, PartialEq)]
pub struct TorrentInfo {
    pub announce: Option<String>,
    pub files: Option<Vec<FileDescription>>,
    pub length: Option<i64>,
    pub name: String,
    pub piece_length: i64,
    pub pieces: Vec<Hash>,
}

use crate::torrent_info_keys::*;

impl Into<Value> for TorrentInfo {
    fn into(self) -> Value {
        let mut result = Dictionary::new();

        if let Some(announce) = self.announce {
            result.insert(KEY_ANNOUNCE.into(), Value::Str(announce.into_bytes()));
        }

        let mut info = Dictionary::new();

        if let Some(files) = self.files {
            let files: Vec<Value> = files.into_iter().map(std::convert::Into::into).collect();
            info.insert(KEY_FILES.into(), Value::List(files));
        }

        if let Some(length) = self.length {
            info.insert(KEY_LENGTH.into(), Value::Int(length));
        }
        info.insert(KEY_NAME.into(), Value::Str(self.name.into_bytes()));
        info.insert(KEY_PIECE_LENGTH.into(), Value::Int(self.piece_length));

        let mut pieces = Vec::new();
        pieces.reserve(self.pieces.len() * std::mem::size_of::<Hash>());
        for p in self.pieces {
            pieces.append(&mut p.to_vec());
        }

        info.insert(KEY_PIECES.into(), Value::Str(pieces));

        result.insert(KEY_INFO.into(), Value::Dict(info));

        Value::Dict(result)
    }
}

impl TryFrom<Value> for TorrentInfo {
    type Error = ParseError;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        let mut value = if let Value::Dict(value) = value {
            Ok(value)
        } else {
            let e =
                InvalidValueTypeError::new(KEY_ANNOUNCE, get_value_type(&value), ValueType::Dict);
            Err(ParseError::InvalidValueType(e))
        }?;

        let announce = if value.contains_key(KEY_ANNOUNCE) {
            Some({
                let announce = value
                    .remove(KEY_ANNOUNCE)
                    .ok_or(ParseError::MissingKey(KEY_ANNOUNCE))?;
                if let Value::Str(announce) = announce {
                    String::from_utf8(announce).or(Err(ParseError::InvalidTextData))
                } else {
                    let e = InvalidValueTypeError::new(
                        KEY_ANNOUNCE,
                        get_value_type(&announce),
                        ValueType::Str,
                    );
                    Err(ParseError::InvalidValueType(e))
                }
            }?)
        } else {
            None
        };

        let info = value
            .remove(KEY_INFO)
            .ok_or(ParseError::MissingKey(KEY_INFO))?;
        let mut info = if let Value::Dict(info) = info {
            Ok(info)
        } else {
            let e =
                InvalidValueTypeError::new(KEY_ANNOUNCE, get_value_type(&info), ValueType::Dict);
            Err(ParseError::InvalidValueType(e))
        }?;

        let files = if let Some(files) = info.remove(KEY_FILES) {
            let files = if let Value::List(files) = files {
                Ok(files)
            } else {
                let e =
                    InvalidValueTypeError::new(KEY_FILES, get_value_type(&files), ValueType::List);
                Err(ParseError::InvalidValueType(e))
            }?;

            let files: ParseResult<Vec<FileDescription>> =
                files.into_iter().map(FileDescription::try_from).collect();
            Some(files?)
        } else {
            None
        };

        let length = if let Some(length) = info.remove(KEY_LENGTH) {
            Some(if let Value::Int(length) = length {
                Ok(length)
            } else {
                let e =
                    InvalidValueTypeError::new(KEY_LENGTH, get_value_type(&length), ValueType::Int);
                Err(ParseError::InvalidValueType(e))
            }?)
        } else {
            None
        };

        let name = parse_text_field(&info, KEY_NAME)?;
        let piece_length = parse_int_field(&info, KEY_PIECE_LENGTH)?;

        let pieces = info
            .remove(KEY_PIECES)
            .ok_or(ParseError::MissingKey(KEY_PIECES))?;
        let pieces = if let Value::Str(pieces) = pieces {
            Ok(pieces)
        } else {
            let e = InvalidValueTypeError::new(KEY_PIECES, get_value_type(&pieces), ValueType::Str);
            Err(ParseError::InvalidValueType(e))
        }?;

        // Validating piece count
        const HASH_SIZE: usize = std::mem::size_of::<Hash>();

        let total_length = if let Some(x) = length {
            Ok(x)
        } else if let Some(ref files) = files {
            Ok(files.iter().fold(0i64, |x, f| x + f.length))
        } else {
            Err(ParseError::MissingKey(KEY_FILES))
        }?;

        let additional_piece = if total_length % piece_length == 0 {
            0
        } else {
            1
        };
        let expected_piece_count = (total_length / piece_length) + additional_piece;
        let actual_piece_count = (pieces.len() / HASH_SIZE) as i64;

        if actual_piece_count != expected_piece_count {
            return Err(ParseError::InvalidPiecesData);
        }

        let pieces = {
            let mut result: Vec<Hash> = Vec::with_capacity(actual_piece_count as usize);

            for i in 0i64..actual_piece_count {
                let offset = i as usize * HASH_SIZE;
                let slice = &pieces[offset..offset + HASH_SIZE];
                let mut hash: Hash = std::default::Default::default();
                hash.copy_from_slice(slice);
                result.push(hash);
            }

            result
        };

        Ok(Self {
            announce,
            files,
            length,
            name,
            piece_length,
            pieces,
        })
    }
}

// For infohash calculation we need to hash raw "info" section from the file, no key reordering
// See: https://stackoverflow.com/questions/19749085/calculating-the-info-hash-of-a-torrent-file#19800109

pub fn compute_info_hash<T>(mut i: T) -> Result<Hash, DecodingError>
where
    T: Iterator<Item = u8> + Clone,
{
    struct Entry<T>
    where
        T: Iterator<Item = u8> + Clone,
    {
        pub value_name: String,
        pub value_start: T,
    }

    //    let mut stack = Vec::new();

    use crate::bencode::markers::*;
    let start_marker = i.next().ok_or(DecodingError::InvalidData)?;
    if start_marker != DICT_VALUE_START {
        return Err(DecodingError::InvalidData);
    }

    for x in i {
        match x {
            INT_VALUE_START => {}
            LIST_VALUE_START => {}
            DICT_VALUE_START => {}
            VALUE_END => {}
            _ => (),
        }
    }

    unimplemented!()
}
