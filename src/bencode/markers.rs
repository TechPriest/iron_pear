pub const VALUE_END: u8 = b'e';
pub const INT_VALUE_START: u8 = b'i';
pub const VALUE_DELIMITER: u8 = b':';
pub const LIST_VALUE_START: u8 = b'l';
pub const DICT_VALUE_START: u8 = b'd';
