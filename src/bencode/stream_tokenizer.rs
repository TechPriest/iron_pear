use crate::bencode::markers;
use std::convert::From;
use std::io::{Error as IoError, Read, Result as IoResult};

#[derive(Debug, PartialEq)]
pub enum Token {
    ValueEnd,
    Int(i64),
    String(Vec<u8>),
    ListStart,
    DictStart,
}

#[derive(Debug)]
pub enum TokenizerError {
    Io(IoError),
    InvalidInput,
}

impl From<IoError> for TokenizerError {
    fn from(e: IoError) -> TokenizerError {
        TokenizerError::Io(e)
    }
}

pub type TokenizerResult<T> = Result<T, TokenizerError>;

#[derive(Debug)]
pub struct StreamTokenizer<R: Read> {
    stream: R,
    #[cfg(test)] offset: u64,
}

impl<R: Read> StreamTokenizer<R> {
    pub fn new(stream: R) -> Self {
        Self {
            stream,
            #[cfg(test)] offset: 0,
        }
    }

    pub fn next_token(&mut self) -> TokenizerResult<Token> {
        let x = self.read_byte()?;

        // String token
        if let Some(d) = Self::decode_digit(x) {
            let len = self.read_string_len(d as usize)?;
            let mut str_data = vec![0u8; len];
            self.stream.read_exact(str_data.as_mut_slice())?;

            #[cfg(test)]
            {
                self.offset += str_data.len() as u64;
                println!("Read str data '{}' @{}", String::from_utf8_lossy(&str_data), self.offset);
            }

            return Ok(Token::String(str_data));
        }

        // Int token
        if x == markers::INT_VALUE_START {
            let v = self.read_int_token()?;
            return Ok(Token::Int(v));
        }

        match x {
            markers::LIST_VALUE_START => Ok(Token::ListStart),
            markers::DICT_VALUE_START => Ok(Token::DictStart),
            markers::VALUE_END => Ok(Token::ValueEnd),
            _ => Err(TokenizerError::InvalidInput),
        }
    }

    fn read_byte(&mut self) -> IoResult<u8> {
        let mut buf = [0u8];
        self.stream.read_exact(&mut buf[..])?;
        Ok(buf[0])
    }

    fn read_string_len(&mut self, first_digit: usize) -> TokenizerResult<usize> {
        let mut result = first_digit;

        loop {
            let x = self.read_byte()?;
            if x == markers::VALUE_DELIMITER {
                return Ok(result);
            }
            let d = Self::decode_digit(x).ok_or(TokenizerError::InvalidInput)?;
            result *= 10;
            result += d as usize;
        }
    }

    fn read_int_token(&mut self) -> TokenizerResult<i64> {
        let mut result = 0i64;

        let mut handle_digit = |x| -> TokenizerResult<()> {
            let d = Self::decode_digit(x).ok_or(TokenizerError::InvalidInput)?;
            result *= 10;
            result += i64::from(d);
            Ok(())
        };

        let x = self.read_byte()?;
        if x == markers::VALUE_END {
            return Ok(0);
        }

        let negate = x == b'-';
        if !negate {
            handle_digit(x)?;
        }

        loop {
            let x = self.read_byte()?;
            if x == markers::VALUE_END {
                break;
            }
            handle_digit(x)?;
        }

        if negate {
            result *= -1;
        }

        Ok(result)
    }

    fn decode_digit(x: u8) -> Option<u8> {
        match x {
            b'0' => Some(0),
            b'1' => Some(1),
            b'2' => Some(2),
            b'3' => Some(3),
            b'4' => Some(4),
            b'5' => Some(5),
            b'6' => Some(6),
            b'7' => Some(7),
            b'8' => Some(8),
            b'9' => Some(9),
            _ => None,
        }
    }

    pub fn stream_mut(&mut self) -> &mut R {
        &mut self.stream
    }
}
