use super::markers::*;
use super::value_decoder::*;

use std::collections::HashMap;
use std::io::Read;
use std::iter::Iterator;

pub type Str = Vec<u8>;
pub type List = Vec<Value>;
pub type Dict = HashMap<String, Value>;

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Int(i64),
    Str(Str),
    List(List),
    Dict(Dict),
}

#[derive(Debug)]
pub enum DecodingError {
    Io(std::io::Error),
    InvalidData,
}

impl Value {
    pub fn decode<R>(r: R) -> DecodingResult<Value>
    where
        R: Read,
    {
        decode_value(r)
    }

    pub fn decode_and_compute_info_hash<R>(r: R) -> DecodingResult<ValueAndInfoHash>
    where
        R: Read,
    {
        decode_value_and_compute_info_hash(r)
    }

    pub fn encode(&self) -> Vec<u8> {
        match self {
            Value::Int(x) => Self::encode_int(*x),
            Value::Str(ref x) => Self::encode_str(x),
            Value::List(ref x) => Self::encode_list(x),
            Value::Dict(ref x) => Self::encode_dict(x),
        }
    }

    pub fn into_text(self) -> Option<String> {
        if let Value::Str(x) = self {
            Some(String::from_utf8_lossy(&x).into())
        } else {
            None
        }
    }

    pub fn into_int(self) -> Option<i64> {
        if let Value::Int(x) = self {
            Some(x)
        } else {
            None
        }
    }

    fn encode_int(x: i64) -> Vec<u8> {
        [INT_VALUE_START]
            .iter()
            .chain(x.to_string().into_bytes().iter())
            .chain([VALUE_END].iter())
            .cloned()
            .collect()
    }

    fn encode_str(x: &[u8]) -> Vec<u8> {
        let l = x.len();

        l.to_string()
            .into_bytes()
            .iter()
            .chain([VALUE_DELIMITER].iter())
            .chain(x.iter())
            .cloned()
            .collect()
    }

    fn encode_list(v: &[Self]) -> Vec<u8> {
        let mut result = Vec::new();

        result.push(LIST_VALUE_START);
        for v in v {
            result.append(&mut v.encode());
        }
        result.push(VALUE_END);

        result
    }

    fn encode_dict(v: &HashMap<String, Self>) -> Vec<u8> {
        let mut result = Vec::new();

        result.push(DICT_VALUE_START);
        for (k, v) in v.iter() {
            result.append(&mut Self::encode_str(k.as_bytes()));
            result.append(&mut v.encode());
        }
        result.push(VALUE_END);

        result
    }
}
