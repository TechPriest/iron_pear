use super::value::*;
use crate::InfoHash;
use std;
use std::io::Read;

const INFO_KEY: &str = "info";

impl std::convert::From<crate::bencode::TokenizerError> for DecodingError {
    fn from(e: crate::bencode::TokenizerError) -> Self {
        use crate::bencode::TokenizerError;

        match e {
            TokenizerError::Io(e) => DecodingError::Io(e),
            TokenizerError::InvalidInput => DecodingError::InvalidData,
        }
    }
}

pub type DecodingResult<T> = std::result::Result<T, DecodingError>;

#[derive(Debug)]
enum DecodingStackEntry {
    DecodingList(List),
    DecodingDictValue((Dict, Option<String>)),
}

trait DecodingListener<R> {
    fn on_decoding_state_changed(&mut self, r: &mut R, stack: &[DecodingStackEntry]);
}

struct NullListener {}

impl<R: Read> DecodingListener<R> for NullListener {
    fn on_decoding_state_changed(&mut self, _r: &mut R, _stack: &[DecodingStackEntry]) {}
}

pub fn decode_value<R: Read>(input: R) -> DecodingResult<Value> {
    let mut listener = NullListener {};
    decode_value_with_listener(input, &mut listener)
}

#[derive(Debug)]
pub struct ValueAndInfoHash {
    pub value: Value,
    pub info_hash: Option<InfoHash>,
}

#[derive(Debug)]
enum InfoHashComputingDecodingListener2State {
    NotStarted,
    InProgress,
    Finished,
}

#[derive(Debug)]
struct InfoHashComputingDecodingListener {
    state: InfoHashComputingDecodingListener2State,
    is_info_read: bool,
}

impl InfoHashComputingDecodingListener {
    pub fn new() -> Self {
        Self {
            state: InfoHashComputingDecodingListener2State::NotStarted,
            is_info_read: false,
        }
    }

    pub fn is_info_read(&self) -> bool {
        self.is_info_read
    }

    fn is_decoding_info(stack: &[DecodingStackEntry]) -> bool {
        if let Some(DecodingStackEntry::DecodingDictValue((_, key))) = stack.first() {
            key.as_ref().map_or(false, |key| key == INFO_KEY)
        } else {
            false
        }
    }
}

type InfoHashComputingRead<R> = crate::utils::HashingRead<R, sha1::Sha1>;

impl<R: Read> DecodingListener<&mut InfoHashComputingRead<R>>
    for InfoHashComputingDecodingListener
{
    fn on_decoding_state_changed(
        &mut self,
        r: &mut &mut InfoHashComputingRead<R>,
        stack: &[DecodingStackEntry],
    ) {
        match self.state {
            InfoHashComputingDecodingListener2State::NotStarted => {
                if Self::is_decoding_info(stack) {
                    r.enable_hashing(true);
                    self.state = InfoHashComputingDecodingListener2State::InProgress;
                }
            }
            InfoHashComputingDecodingListener2State::InProgress => {
                if !Self::is_decoding_info(stack) {
                    r.enable_hashing(false);
                    self.state = InfoHashComputingDecodingListener2State::Finished;
                    self.is_info_read = true; // Info section was read completely
                }
            }
            InfoHashComputingDecodingListener2State::Finished => (),
        }
    }
}

#[test]
fn computes_info_hash_while_decoding_simple_dict() {
    use hex_literal::hex;

    let data = b"d4:infoi42ee";
    let cursor = std::io::Cursor::new(data);
    let mut reader = InfoHashComputingRead::new(cursor, sha1::Sha1::default(), false);
    let mut listener = InfoHashComputingDecodingListener::new();
    let _ = decode_value_with_listener(reader.by_ref(), &mut listener).unwrap();
    let (hash, _) = reader.finish();

    assert!(listener.is_info_read());
    assert_eq!(
        hash[..],
        hex!("3ce69356df4222111c27b41cccf2164e6cced799")[..]
    );
}

#[test]
fn computes_info_hash_while_decoding_nested_dict() {
    use hex_literal::hex;

    let data = b"d4:infod3:fooi42eee"; // b93566ab8bc18668517b0e6a573ede4c90eb43bd
    let cursor = std::io::Cursor::new(data);
    let mut reader = InfoHashComputingRead::new(cursor, sha1::Sha1::default(), false);
    let mut listener = InfoHashComputingDecodingListener::new();
    let _ = decode_value_with_listener(reader.by_ref(), &mut listener).unwrap();
    let (hash, _) = reader.finish();

    assert!(listener.is_info_read());
    assert_eq!(
        hash[..],
        hex!("b93566ab8bc18668517b0e6a573ede4c90eb43bd")[..]
    );
}

pub fn decode_value_and_compute_info_hash<R: Read>(input: R) -> DecodingResult<ValueAndInfoHash> {
    let mut hashing_read = InfoHashComputingRead::new(input, sha1::Sha1::default(), false);
    let mut listener = InfoHashComputingDecodingListener::new();
    let value = decode_value_with_listener(hashing_read.by_ref(), &mut listener)?;
    let info_hash = if listener.is_info_read() {
        let (hash, _) = hashing_read.finish();
        let mut info_hash = crate::InfoHash::default();
        for (i, v) in hash.iter().enumerate() {
            info_hash.data[i] = *v;
        }

        Some(info_hash)
    } else {
        None
    };

    Ok(ValueAndInfoHash { value, info_hash })
}

fn decode_value_with_listener<R: Read, Listener: DecodingListener<R>>(
    input: R,
    listener: &mut Listener,
) -> DecodingResult<Value> {
    use crate::bencode::StreamTokenizer;
    let mut tokenizer = StreamTokenizer::new(input);
    decode_value_impl(&mut tokenizer, listener)
}

fn decode_value_impl<R: Read, L: DecodingListener<R>>(
    tokenizer: &mut crate::bencode::StreamTokenizer<R>,
    listener: &mut L,
) -> DecodingResult<Value> {
    use crate::bencode::*;

    let mut stack = Vec::new();

    loop {
        let token = tokenizer.next_token()?;

        let current_value = match token {
            Token::Int(x) => {
                Value::Int(x)
            }
            Token::String(x) => {
                Value::Str(x)
            }
            Token::ListStart => {
                let lst = List::new();
                stack.push(DecodingStackEntry::DecodingList(lst));
                listener.on_decoding_state_changed(tokenizer.stream_mut(), &stack);
                continue;
            }
            Token::DictStart => {
                let dict = Dict::new();
                stack.push(DecodingStackEntry::DecodingDictValue((dict, None)));
                listener.on_decoding_state_changed(tokenizer.stream_mut(), &stack);
                continue;
            }
            Token::ValueEnd => { 
                let parent_state = stack.pop().ok_or(DecodingError::InvalidData)?;

                match parent_state {
                    DecodingStackEntry::DecodingList(lst) => Value::List(lst),
                    DecodingStackEntry::DecodingDictValue((d, _)) => Value::Dict(d),
                }                
            }
        };

        if let Some(parent_state) = stack.last_mut() {
            match parent_state {
                DecodingStackEntry::DecodingList(lst) => {
                    lst.push(current_value);
                }
                DecodingStackEntry::DecodingDictValue((d, k)) => {
                    if let Some(k) = k.take() {
                        d.insert(k, current_value);
                    } else if let Value::Str(next_key) = current_value {
                        let next_key = String::from_utf8(next_key).or(Err(DecodingError::InvalidData))?;
                        *k = Some(next_key);
                    } else {
                        return Err(DecodingError::InvalidData);
                    }
                }
            }
            listener.on_decoding_state_changed(tokenizer.stream_mut(), &stack);
        } else {
            return Ok(current_value);
        }
    }
}

#[cfg(test)]
fn decode_test_data(src: &[u8]) -> DecodingResult<Value> {
    use std::io::Cursor;

    let stream = Cursor::new(src);
    decode_value(stream)
}

#[test]
fn decodes_single_int_value() {
    let data = b"i13e";
    let value = decode_test_data(data).unwrap();
    assert_eq!(value, Value::Int(13));
}

#[test]
fn decodes_single_string_value() {
    let data = b"3:foo";
    let value = decode_test_data(data).unwrap();
    assert_eq!(value, Value::Str(b"foo".to_vec()));
}

#[test]
fn decodes_simple_list_value() {
    let data = b"li1ei2ee";
    let value = decode_test_data(data).unwrap();
    if let Value::List(x) = value {
        assert_eq!(x[0], Value::Int(1));
        assert_eq!(x[1], Value::Int(2));
    } else {
        panic!("Invalid value type");
    }
}

#[test]
fn decodes_simple_dict_value() {
    let data = b"d3:fooi42e3:bari13ee";
    let value = decode_test_data(data).unwrap();
    if let Value::Dict(x) = value {
        let v = x.get("foo").unwrap();
        assert_eq!(*v, Value::Int(42));
        let v = x.get("bar").unwrap();
        assert_eq!(*v, Value::Int(13));
    } else {
        panic!("Invalid value type");
    }
}

#[test]
fn decodes_nested_list_value() {
    let data = b"lli1ei2eee";
    let value = decode_test_data(data).unwrap();
    if let Value::List(x) = value {
        assert_eq!(1, x.len());

        if let Value::List(x) = &x[0] {
            assert_eq!(2, x.len());
            assert_eq!(x[0], Value::Int(1));
            assert_eq!(x[1], Value::Int(2));
        } else {
            panic!("Invalid nested value type");
        }
    } else {
        panic!("Invalid value type");
    }
}

#[cfg(test)]
struct TestDecodingListener {}

#[cfg(test)]
impl<R: Read> DecodingListener<R> for TestDecodingListener {
    fn on_decoding_state_changed(&mut self, _r: &mut R, _stack: &[DecodingStackEntry]) {}
}

#[test]
fn decodes_nested_dict_value() {
    let data = b"d3:food3:bari42eee";
    let cursor = std::io::Cursor::new(data);
    let mut listener = TestDecodingListener {};
    let value = decode_value_with_listener(cursor, &mut listener).unwrap();

    if let Value::Dict(x) = value {
        if let Value::Dict(x) = x.get("foo").unwrap() {
            assert_eq!(*x.get("bar").unwrap(), Value::Int(42));
        } else {
            panic!("Invalid nested value type");
        }
    } else {
        panic!("Invalid value type");
    }
}

#[test]
fn decodes_dict_with_list_value() {
    let data = b"d3:fool3:bar3:bazee";
    let cursor = std::io::Cursor::new(data);
    let mut listener = TestDecodingListener {};
    let _value = decode_value_with_listener(cursor, &mut listener).unwrap();
}

#[test]
fn decodes_nested_dict_with_list_value() {
    let data = b"d4:infod3:fool3:bar3:bazeee";
    let cursor = std::io::Cursor::new(data);
    let mut listener = TestDecodingListener {};
    let _value = decode_value_with_listener(cursor, &mut listener).unwrap();
}

#[test]
fn decodes_dict_of_lists_of_dicts() {
    let data = b"d5:filesld6:lengthi1656320e4:pathl3:adb7:adb.exeeed6:lengthi851456e4:pathl8:fastboot12:fastboot.exeeeee";
    let cursor = std::io::Cursor::new(&data[..]);
    let mut listener = TestDecodingListener {};
    let _value = decode_value_with_listener(cursor, &mut listener).unwrap();
}

#[test]
fn decodes_empty_list_value() {
    let data = b"le";
    let value = decode_test_data(data).unwrap();

    if let Value::List(x) = value {
        assert!(x.is_empty());
    } else {
        panic!("Invalid value type");
    }
}

#[test]
fn decodes_empty_dict_value() {
    let data = b"de";
    let value = decode_test_data(data).unwrap();

    if let Value::Dict(x) = value {
        assert!(x.is_empty());
    } else {
        panic!("Invalid value type");
    }
}

#[test]
fn decodes_torrent_and_computes_info_hash() {
    // for reference: https://stackoverflow.com/questions/28140766/hash-calculation-in-torrent-clients
    // Info_hash is a hash of "info" value in root dict

    use hex_literal::hex;

    let data = include_bytes!("../tests/data/lipsum.txt.torrent"); //FEF50EC327459C1236CF45E6F58D9493ADB3DFD2
    let cursor = std::io::Cursor::new(&data[..]);
    let x = decode_value_and_compute_info_hash(cursor).unwrap();
    let info_hash = x.info_hash.unwrap();
    assert_eq!(
        info_hash.data[..],
        hex!("FEF50EC327459C1236CF45E6F58D9493ADB3DFD2")[..]
    );
}
