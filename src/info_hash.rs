use crate::net_serialize::{NetRead, NetWrite};
use std::io::{Read, Result as IoResult, Write};

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct InfoHash {
    pub data: [u8; 20],
}

impl NetWrite for InfoHash {
    fn write_to<W: Write>(&self, w: &mut W) -> IoResult<()> {
        w.write_all(&self.data)
    }
}

impl NetRead for InfoHash {
    fn read_from<R: Read>(r: &mut R) -> IoResult<Self> {
        let mut result = Self::default();
        r.read_exact(&mut result.data)?;
        Ok(result)
    }
}
