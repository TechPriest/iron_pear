pub const KEY_ANNOUNCE: &str = "announce";
pub const KEY_INFO: &str = "info";
pub const KEY_FILES: &str = "files";
pub const KEY_NAME: &str = "name";
pub const KEY_PIECE_LENGTH: &str = "piece length";
pub const KEY_PIECES: &str = "pieces";
