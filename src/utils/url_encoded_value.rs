#[derive(Debug, Clone)]
pub struct UrlEncodedValue {
    value: String,
}

impl UrlEncodedValue {
    pub fn new(value: &[u8]) -> Self {
        Self {
            value: super::url_encode(value),
        }
    }

    pub fn value(&self) -> &str {
        &self.value
    }
}
