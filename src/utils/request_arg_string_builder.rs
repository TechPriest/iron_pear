use std::string::ToString;

#[derive(Debug, Default)]
pub struct RequestArgStringBuilder {
    value: String,
}

impl RequestArgStringBuilder {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            value: String::with_capacity(capacity),
        }
    }

    const KV_SEPARATOR: char = '=';
    const SEPARATOR: char = '&';

    pub fn append_str(&mut self, k: &str, v: &str) {
        if !self.value.is_empty() {
            self.value.push(Self::SEPARATOR);
        }

        self.value.push_str(k);
        self.value.push(Self::KV_SEPARATOR);
        self.value.push_str(v);
    }

    pub fn append<T>(&mut self, k: &str, v: &T)
    where
        T: ToString,
    {
        self.append_str(k, &v.to_string());
    }
}

impl Into<String> for RequestArgStringBuilder {
    fn into(self) -> String {
        self.value
    }
}
