use digest::Digest;
use generic_array::GenericArray;
use std::io::{Read, Result as IoResult};

pub struct HashingRead<R, H> {
    read: R,
    hash: H,
    hashing_enabled: bool,
}

impl<R, H> HashingRead<R, H>
where
    R: Read,
    H: Digest,
{
    pub fn new(read: R, hash: H, enable_hashing: bool) -> Self {
        Self {
            read,
            hash,
            hashing_enabled: enable_hashing,
        }
    }

    pub fn finish(self) -> (GenericArray<u8, H::OutputSize>, R) {
        (self.hash.finalize(), self.read)
    }

    /// Updates hash with sideloaded data, read operations are not affected
    pub fn feed_data(&mut self, data: &[u8]) {
        self.hash.update(data);
    }

    pub fn enable_hashing(&mut self, enable: bool) {
        self.hashing_enabled = enable;
    }
}

impl<R, H> Read for HashingRead<R, H>
where
    R: Read,
    H: Digest,
{
    fn read(&mut self, data: &mut [u8]) -> IoResult<usize> {
        let size = self.read.read(data)?;
        if self.hashing_enabled && size > 0 {
            let data = &data[..size];
            self.feed_data(data);
        }
        Ok(size)
    }
}

#[test]
fn hashes_input() {
    use hex_literal::*;
    use sha1::{Digest, Sha1};
    use std::io::Cursor;

    let read = Cursor::new(b"hello world");
    let hash = Sha1::new();

    let mut hr = HashingRead::new(read, hash, true);
    let mut buf = Vec::new();
    let size = hr.read_to_end(&mut buf).unwrap();
    assert_eq!(11, size);
    let (result, _) = hr.finish();
    assert_eq!(result[..], hex!("2aae6c35c94fcfb415dbe95f408b9ce91ee846ed"));
}
