mod hashing_read;
mod request_arg_string_builder;
mod url_encoded_value;

pub use self::hashing_read::*;
pub use self::request_arg_string_builder::*;
pub use self::url_encoded_value::*;

pub type Base32EncodingAlphabet = [char; 32];

// RFC 4648, 5 bits per character
const BASE32_ALPHABET: Base32EncodingAlphabet = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7',
];

// Non-standard version used by torrent clients for magnet link representation
const BASE32_LOWERCASE_ALPHABET: Base32EncodingAlphabet = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6', '7',
];

const BASE32_PADDING_CHAR: char = '=';
const BASE32_CHUNK_SIZE: usize = 8;

fn base32_encoded_data_len(input_len: usize) -> usize {
    // Each Base32 character represents 5 bits of information
    let n = input_len * 8 / 5;
    let n_chunks = n / BASE32_CHUNK_SIZE;

    if n % BASE32_CHUNK_SIZE == 0 {
        n_chunks * BASE32_CHUNK_SIZE
    } else {
        (n_chunks + 1) * BASE32_CHUNK_SIZE
    }
}

#[test]
fn correctly_computes_encoded_data_len() {
    assert_eq!(0, base32_encoded_data_len(0));
    assert_eq!(8, base32_encoded_data_len(1));
}

fn base32_encode_with_alphabet(data: &[u8], alphabet: &Base32EncodingAlphabet) -> String {
    let len = base32_encoded_data_len(data.len());
    let mut result = Vec::with_capacity(len);
    let mut iter = data.iter().cloned();

    // Well, here s bit slicing order is wrong
    // Top bits should be taken first
    loop {
        const MAX_DATA_MASK: u8 = 0b1111_1000;
        const DATA_SHIFT: u8 = 3;

        // First 5 bits, 3 bits overflow
        let overflow = if let Some(b) = iter.next() {
            const MASK: u8 = MAX_DATA_MASK;
            let (data, overflow) = ((b & MASK) >> DATA_SHIFT, (b & !MASK) << 5);
            result.push(alphabet[data as usize]); // 1
            overflow
        } else {
            break;
        };

        // 10 bits, 1 bit overflow
        let overflow = if let Some(b) = iter.next() {
            const MASK: u8 = 0b1100_0000;
            let data = (overflow | ((b & MASK) >> 3)) >> DATA_SHIFT;
            let overflow = (b & !MASK) << 2; // 6 bits still left
            result.push(alphabet[data as usize]); // 2

            let data = (overflow & MAX_DATA_MASK) >> DATA_SHIFT;
            let overflow = overflow << 5;
            result.push(alphabet[data as usize]); // 3
            overflow
        } else {
            result.push(alphabet[(overflow >> DATA_SHIFT) as usize]);
            break;
        };

        // 5 bits, 4 bits overflow
        let overflow = if let Some(b) = iter.next() {
            const MASK: u8 = 0b1111_0000;
            let data = (overflow | ((b & MASK) >> 1)) >> DATA_SHIFT;
            let overflow = (b & !MASK) << 4;
            result.push(alphabet[data as usize]); // 4
            overflow
        } else {
            if overflow != 0 {
                result.push(alphabet[(overflow >> DATA_SHIFT) as usize]);
            }
            break;
        };

        // 10 bits, 2 bits overflow
        let overflow = if let Some(b) = iter.next() {
            const MASK: u8 = 0b1000_0000;
            let data = (overflow | ((b & MASK) >> 4)) >> DATA_SHIFT;
            let overflow = (b & !MASK) << 1;
            result.push(alphabet[data as usize]); // 5
            let data = (overflow & MAX_DATA_MASK) >> DATA_SHIFT;
            let overflow = b << 6;
            result.push(alphabet[data as usize]); // 6
            overflow
        } else {
            if overflow != 0 {
                result.push(alphabet[(overflow >> DATA_SHIFT) as usize]);
            }
            break;
        };

        // 10 bits
        if let Some(b) = iter.next() {
            const MASK: u8 = 0b1110_0000;
            let data = (overflow | ((b & MASK) >> 2)) >> DATA_SHIFT;
            result.push(alphabet[data as usize]); // 7
            let data = b & 0b0001_1111;
            result.push(alphabet[data as usize]); // 8
        } else {
            if overflow != 0 {
                result.push(alphabet[(overflow >> DATA_SHIFT) as usize]);
            }
            break;
        }
    }

    let padding_len = len - result.len();
    result
        .into_iter()
        .chain(std::iter::repeat(BASE32_PADDING_CHAR).take(padding_len))
        .collect()
}

pub fn base32_encode(data: &[u8]) -> String {
    base32_encode_with_alphabet(data, &BASE32_ALPHABET)
}

pub fn base32_encode_lowercase(data: &[u8]) -> String {
    base32_encode_with_alphabet(data, &BASE32_LOWERCASE_ALPHABET)
}

#[test]
fn base32_encodes_simple_values() {
    // Test values are encoded using https://emn178.github.io/online-tools/base32_encode.html
    assert_eq!("ME======", base32_encode(b"a"));
    assert_eq!("MFQQ====", base32_encode(b"aa"));
    assert_eq!("MFQWC===", base32_encode(b"aaa"));
    assert_eq!("MFQWCYI=", base32_encode(b"aaaa"));
    assert_eq!("MFQWCYLB", base32_encode(b"aaaaa"));
    assert_eq!("MFQWCYLBME======", base32_encode(b"aaaaaa"));
    assert_eq!("LJNFUWS2LI======", base32_encode(b"ZZZZZZ"));
}

const BASE16_ALPHABET: [char; 16] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
];

fn base16_encode_byte(b: u8) -> (char, char) {
    let h = b >> 4;
    let l = b & 0b0000_1111;
    (BASE16_ALPHABET[h as usize], BASE16_ALPHABET[l as usize])
}

pub fn base16_encode(data: &[u8]) -> String {
    let mut result = String::with_capacity(data.len() * 2);

    for b in data {
        let (h, l) = base16_encode_byte(*b);
        result.push(h);
        result.push(l);
    }

    result
}

#[test]
fn base16_encodes_simple_values() {
    assert_eq!("00", base16_encode(&[0u8]));
    assert_eq!("FF", base16_encode(&[255u8]));
}

fn is_uri_unreserved_char(b: u8) -> bool {
    // https://en.wikipedia.org/wiki/Percent-encoding#Types_of_URI_characters

    const UPPERCASE_LETTERS_START: u8 = b'A';
    const UPPERCASE_LETTERS_END: u8 = b'Z';
    const LOWERCASE_LETTERS_START: u8 = b'a';
    const LOWERCASE_LETTERS_END: u8 = b'z';
    const DIGITS_START: u8 = b'0';
    const DIGITS_END: u8 = b'9';
    const DASH: u8 = b'-';
    const UNDERSCORE: u8 = b'_';
    const PERIOD: u8 = b'.';
    const TILDE: u8 = b'~';

    match b {
        UPPERCASE_LETTERS_START..=UPPERCASE_LETTERS_END => true,
        LOWERCASE_LETTERS_START..=LOWERCASE_LETTERS_END => true,
        DIGITS_START..=DIGITS_END => true,
        DASH | UNDERSCORE | PERIOD | TILDE => true,
        _ => false,
    }
}

pub fn url_encode(data: &[u8]) -> String {
    let mut result = String::with_capacity(data.len() * 3);

    for b in data {
        if is_uri_unreserved_char(*b) {
            result.push(*b as char);
        } else {
            result.push('%');
            let (h, l) = base16_encode_byte(*b);
            result.push(h);
            result.push(l);
        }
    }
    result
}

#[test]
fn url_encodes_simple_data() {
    assert_eq!(
        "%23FOO%60bar%2F123%2Bbaz%24",
        url_encode(b"#FOO`bar/123+baz$")
    );
}
