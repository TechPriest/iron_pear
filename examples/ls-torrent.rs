use iron_pear::bencode::Value;
use std::convert::TryFrom;
use std::io::{Cursor, Read};
use std::fs::File;
use easy_http_request::DefaultHttpRequest;
use structopt::StructOpt;
use pad::PadStr;
use iron_pear::torrent_info::TorrentInfo;
use iron_pear::utils;

fn humanize_length(x: i64) -> String {
    const KB: i64 = 1024;
    const MB: i64 = 1024 * KB;
    const GB: i64 = 1024 * MB;
    const TB: i64 = 1024 * GB;

    fn format_length(x: i64, unit_length: i64, suffix: &str) -> String {
        if unit_length != 1 {
            let x = x as f64 / unit_length as f64;
            format!("{:.*} {}", 2, x, suffix)
        } else {
            format!("{} {}", x, suffix)
        }
    }

    let (unit_length, suffix) = if x >= TB {
        (TB, "TB")
    } else if x >= GB {
        (GB, "GB")
    } else if x > MB {
        (MB, "MB")
    } else if x > KB {
        (KB, "KB")
    } else {
        (1, "B")
    };

    format_length(x, unit_length, suffix)
}

fn is_url(s: &str) -> bool {
    s.contains("://")
}

fn load_blob(location: &str) -> Vec<u8> {
    if is_url(location) {
        let response = DefaultHttpRequest::get_from_url_str(location)
            .expect("Failed to create request")
            .send()
            .expect("Failed to send request");
        response.body
    } else {
        let mut buf = Vec::new();
        let mut f = File::open(location)
            .expect("Failed to open specified file");
        f.read_to_end(&mut buf)
            .expect("Failed to read file data");
        buf
    }
}

/// This program reads contents of a .torrent file
#[derive(Debug, StructOpt)]
struct Options {
    /// Print version and exit
    #[structopt(short="v", long="version")]
    print_version: bool,

    /// Print human-readable file lengths
    #[structopt(short="H", long)]
    humanize: bool,

    /// Examined file location, can be file path or HTTP(s) URL
    torrent_file_location: String,
}

fn main() {
    let options = Options::from_args();
    if options.print_version {
        println!("{}", env!("CARGO_PKG_VERSION").to_string());
        return;
    }

    println!(r#"Torrent:	{0}"#, &options.torrent_file_location);
    let data = load_blob(&options.torrent_file_location);
    let value_and_hash = Value::decode_and_compute_info_hash(Cursor::new(data))
        .expect("Unable to parse bencoded data");
    if let Some(info_hash) = value_and_hash.info_hash {
        println!(r#"info_hash:	{}"#, utils::base16_encode(&info_hash.data));
        println!(
            "magnet link:\tmagnet:?xt=urn:btih:{}",
            utils::base32_encode_lowercase(&info_hash.data)
        );
    }

    let info = TorrentInfo::try_from(value_and_hash.value).expect("Unable to parse TorrentInfo");

    if let Some(announce) = info.announce {
        println!("Announce URL:\t{}", announce);
    }

    struct Entry {
        name: String,
        length: i64,
    }

    let name = info.name;
    let entries = if let Some(files) = info.files {
        let separator = std::path::MAIN_SEPARATOR.to_string();
        files
            .into_iter()
            .map(|f| {
                let name = f
                    .path
                    .into_iter()
                    .fold(name.clone(), |path, p| path + &separator + &p);
                Entry {
                    name,
                    length: f.length,
                }
            })
            .collect()
    } else {
        let length = info.length.unwrap_or_default();
        vec![Entry { name, length }]
    };

    let max_name_len = entries.iter().fold(0usize, |max_name_len, e| {
        let len = e.name.len();
        if len > max_name_len {
            len
        } else {
            max_name_len
        }
    });
    let pad_len = 4 - (max_name_len % 4);
    let name_padding = max_name_len + pad_len;

    println!(
        "{name}Length",
        name = "Name".to_string().pad_to_width(name_padding)
    );
    println!(
        "{name}------",
        name = "----".to_string().pad_to_width(name_padding)
    );

    for entry in entries {
        let length = if options.humanize {
            humanize_length(entry.length)
        } else {
            entry.length.to_string()
        };
        println!(
            "{name}{length}",
            name = entry.name.pad_to_width(name_padding),
            length = length
        );
    }
}
