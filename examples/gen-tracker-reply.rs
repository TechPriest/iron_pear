use structopt::StructOpt;
use iron_pear::tracker::CompactIpv4PeerDescription;
use iron_pear::bencode::{Dict, Value};
use iron_pear::tracker::response_keys;
use std::net::{Ipv4Addr, SocketAddrV4};
use std::path::PathBuf;
use std::fs::File;
use std::io::Write;

fn parse_v4_peer_description(s: &str) -> Result<CompactIpv4PeerDescription, &'static str> {
    const ERROR_DESCR: &str = "Invalid peer address format";
    let mut split = s.splitn(2, ':');
    let addr_str = split.next().ok_or(ERROR_DESCR)?;
    let port_str = split.next().ok_or(ERROR_DESCR)?;
    
    let mut split = addr_str.splitn(4, '.');
    let mut parse_octet = move || {
        split.next()
            .ok_or(ERROR_DESCR)
            .and_then(|x| x.parse::<u8>().map_err(|_| ERROR_DESCR))
    };

    let a = parse_octet()?;
    let b = parse_octet()?;
    let c = parse_octet()?;
    let d = parse_octet()?;
    let ip_addr = Ipv4Addr::new(a, b, c, d);

    let port: u16 = port_str.parse().map_err(|_| ERROR_DESCR)?;

    Ok(CompactIpv4PeerDescription{
        addr: SocketAddrV4::new(ip_addr, port)
    })
}

#[test]
fn parses_peer_description_str() {
    let src = "101.202.133.244:4213";
    let peer_descr = parse_v4_peer_description(src).unwrap();
    assert_eq!(peer_descr.addr.ip().octets(), [101, 202, 133, 244]);
    assert_eq!(peer_descr.addr.port(), 4213);
}

fn encode_peer(x: &CompactIpv4PeerDescription) -> [u8; 6] {
    let mut result = [0u8; 6];
    (&mut result[..4]).clone_from_slice(&x.addr.ip().octets()[..]);
    let port = x.addr.port();
    result[4] = ((port & 0xFF00) >> 8) as u8;
    result[5] = (port & 0xFF) as u8;
    result
}

#[test]
fn encodes_peer() {
    let peer = CompactIpv4PeerDescription{
        addr: SocketAddrV4::new(Ipv4Addr::new(101, 202, 133, 244), 4213)
    };
    let encoded_peer = encode_peer(&peer);
    assert_eq!(encoded_peer, [101, 202, 133, 244, 0x10, 0x75]);
}

fn encode_peers(src: &[CompactIpv4PeerDescription]) -> Value {
    let mut peers = Vec::with_capacity(src.len() * 6);
    for p in src.iter().map(encode_peer) {
        peers.extend(&p[..]);
    }
    Value::Str(peers)
}

/// Generates compact tracker response file
#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(short, long)]
    output_path: PathBuf,

    #[structopt(short, long, default_value="100")]
    interval: i32,

    #[structopt(parse(try_from_str = parse_v4_peer_description))]
    peers: Vec<CompactIpv4PeerDescription>,
}

fn main() {
    let options = Options::from_args();
    let mut d = Dict::new();
    d.insert(response_keys::INTERVAL.into(), Value::Int(options.interval as i64));
    d.insert(response_keys::PEERS.into(), encode_peers(options.peers.as_slice()));

    let mut f = File::create(options.output_path).expect("Failed to open output file");
    f.write_all(Value::Dict(d).encode().as_slice()).expect("Failed to write data");
}