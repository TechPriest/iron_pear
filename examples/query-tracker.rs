use iron_pear::bencode::Value;
use iron_pear::peer_id::PeerId;
use iron_pear::torrent_info::TorrentInfo;
use iron_pear::tracker::udp_tracker;
use iron_pear::tracker::*;
use iron_pear::utils::UrlEncodedValue;
use iron_pear::InfoHash;
use rand::prelude::*;
use std::convert::TryFrom;
use std::fmt::Display;
use std::fs::File;
use std::io::{BufReader, Cursor, Read};
use std::time::Duration;
use structopt::StructOpt;
use easy_http_request::DefaultHttpRequest;
use url::Url;

fn parse_event_name(s: &str) -> std::result::Result<TrackerEvent, &'static str> {
    use request_keys::event_names;

    let table = [
        (event_names::EVENT_STARTED, TrackerEvent::Started),
        (event_names::EVENT_STOPPED, TrackerEvent::Stopped),
        (event_names::EVENT_COMPLETED, TrackerEvent::Completed),
    ];

    Ok(table
        .iter()
        .find(|entry| entry.0 == s)
        .ok_or("Unexpected event name")?
        .1)
}

fn query_http_tracker(
    url: &str,
    request: HttpTrackerRequest,
    info_hash: &UrlEncodedInfoHash,
) -> TrackerResponse {
    let url = Url::parse(url).expect("Invalid tracker URL");
    let url = request.build_request_url(info_hash, url);
    let request_url = url.into_string();
    println!("HTTP tracker request: {}", &request_url);

    let response = DefaultHttpRequest::get_from_url_str(request_url)
        .expect("Failed to create HTTP request")
        .send()
        .expect("Failed to send HTTP request");

    println!(
        "Response status: {}",
        response.status_code,
    );

    let value = Value::decode(Cursor::new(response.body)).expect("Failed to decode tracker response");
    parse_tracker_response(value).expect("Failed to parse tracker response")
}

fn random_port() -> u16 {
    thread_rng().gen_range(49152 .. 65535)
}

fn query_udp_tracker(url: &str, request: &TrackerRequestBase, info_hash: &InfoHash) {
    use std::net::{
        Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6, ToSocketAddrs, UdpSocket,
    };

    let host_port = udp_tracker::parse_udp_tracker_url(url).unwrap();
    let tracker_addr = host_port
        .to_socket_addrs()
        .expect("Failed to resolve tracker address")
        .next()
        .unwrap();

    println!("Querying UDP tracker {} -> {:?}", &host_port, &tracker_addr);

    const MAX_RETRIES: u32 = 5;
    fn do_exchange<'a>(s: &UdpSocket, data: &[u8], buf: &'a mut [u8]) -> &'a [u8] {
        for _ in 0..MAX_RETRIES {
            s.send(data).expect("Failed to send data");

            if let Ok(size) = s.recv(buf) {
                return &buf[..size];
            }
        }
        panic!("Failed to perform data exchange");
    }

    const READ_TIMEOUT: Option<Duration> = Some(Duration::from_secs(5));
    let port = random_port();

    let do_connect = |sock: &UdpSocket, buf: &mut [u8]| -> u64 {
        println!("Establishing connection...");
        let transaction_id = thread_rng().next_u32();
        let conn_request = udp_tracker::make_connection_request(transaction_id);
        let conn_resp = do_exchange(&sock, &conn_request, buf);
        let conn_resp = udp_tracker::parse_connection_response(conn_resp)
            .expect("Failed to parse connection response");
        if conn_resp.transaction_id != transaction_id {
            panic!("TransactionID mismatch");
        }
        conn_resp.connection_id
    };

    fn print_peers<T: Display>(peers: &[T]) {
        println!("Peers:");
        for p in peers {
            println!("\t{}", p);
        }
    }

    let make_announce_request =
        |connection_id: u64, transaction_id: u32| udp_tracker::AnnounceRequest {
            connection_id,
            transaction_id,
            info_hash,
            peer_id: request.peer_id,
            downloaded: request.downloaded,
            left: request.left,
            uploaded: request.uploaded,
            event: None,
            key: 0,
            num_want: request.numwant,
            port,
        };

    let do_announce = |my_addr: SocketAddr, tracker_addr: SocketAddr| {
        let s = UdpSocket::bind(my_addr).expect("Failed to bind UDP socket");
        s.connect(tracker_addr)
            .expect("Failed to `connect` to tracker");
        s.set_read_timeout(READ_TIMEOUT)
            .expect("Failed to set read timeout");

        let mut buf = [0u8; udp_tracker::MAX_UDP_DATA_SIZE];
        let connection_id = do_connect(&s, &mut buf);
        let transaction_id = thread_rng().next_u32();

        let announce_request = make_announce_request(connection_id, transaction_id);

        let announce_resp = do_exchange(&s, &announce_request.make_ipv4_request(None), &mut buf);
        let announce_resp = udp_tracker::parse_ipv4_announce_response(announce_resp)
            .expect("Failed to parse announce response");

        if announce_resp.common.transaction_id != transaction_id {
            panic!("TransactionID mismatch");
        }

        println!("Interval: {} sec", announce_resp.common.interval);
        println!("Leechers: {}", announce_resp.common.leechers);
        println!("Seeders: {}", announce_resp.common.seeders);
        print_peers(&announce_resp.peers);
    };

    match tracker_addr {
        SocketAddr::V4(_) => {
            let my_addr = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), port);
            do_announce(my_addr.into(), tracker_addr);
        }
        SocketAddr::V6(_) => {
            let my_addr = SocketAddrV6::new(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0), port, 0, 0);
            do_announce(my_addr.into(), tracker_addr);
        }
    }
}

fn main() {
    #[derive(Debug, StructOpt)]
    struct Options {
        torrent_file: String,

        #[structopt(
            short = "p",
            long,
            help = "Port for incoming peer connections, random if left unspecified"
        )]
        port: Option<u16>,

        #[structopt(
            short = "C",
            long,
            help = "Request peer list on non-compact form, most trackers ignore this option"
        )]
        non_compact: bool,

        #[structopt(
            short = "u",
            long,
            default_value = "0",
            help = "Amount of uploaded data in bytes to report"
        )]
        uploaded: u64,

        #[structopt(
            short = "d",
            long,
            default_value = "0",
            help = "Amount of downloaded data in bytes to report"
        )]
        downloaded: u64,

        #[structopt(
            short = "l",
            long,
            default_value = "0",
            help = "Amount of remaining data in bytes to report"
        )]
        left: u64,

        #[structopt(
            short,
            long,
            parse(try_from_str=parse_event_name),
            help = "Event to report to tracker"
        )]
        event: Option<TrackerEvent>,

        #[structopt(short = "w", long, help = "Number of peers to request")]
        numwant: Option<i32>,

        #[structopt(short, long)]
        ip: Option<String>,

        #[structopt(short, long)]
        tracker: Option<String>,
    }

    let options = Options::from_args();
    println!("Torrent file: {}", &options.torrent_file);
    let f = File::open(options.torrent_file).expect("Unable to open torrent file");
    let mut reader = BufReader::new(f);
    let mut data = Vec::new();
    reader
        .read_to_end(&mut data)
        .expect("Unable to read data from file");

    let value_and_hash = Value::decode_and_compute_info_hash(Cursor::new(data))
        .expect("Unable to parse bencoded data");

    let info_hash = value_and_hash
        .info_hash
        .expect("Failed to compute info_hash for specified torrent file");
    let info = TorrentInfo::try_from(value_and_hash.value).expect("Unable to parse TorrentInfo");
    let announce_url = options.tracker.or( info.announce)
        .expect("Torrent does not contain announce URL and no tracker specified");
    let peer_id = PeerId::generate();
    let request_base = TrackerRequestBase {
        peer_id: &peer_id,
        port: options.port.unwrap_or_else(random),
        uploaded: options.uploaded,
        downloaded: options.downloaded,
        left: options.left,
        numwant: options.numwant,
    };

    let is_udp_tracker = announce_url.starts_with(udp_tracker::URL_PREFIX);
    if !is_udp_tracker {
        let encoded_info_hash = UrlEncodedValue::new(&info_hash.data);
        let tracker_request = HttpTrackerRequest {
            base: request_base,
            compact: !options.non_compact,
            event: options.event,
            ip: options.ip.as_ref().map(String::as_str),
            key: None,
            tracker_id: None,
        };

        let tracker_resp = query_http_tracker(&announce_url, tracker_request, &encoded_info_hash);
        let tracker_resp = match tracker_resp {
            TrackerResponse::Success(r) => r,
            TrackerResponse::Failure(msg) => {
                panic!("Tracker reported failure: {}", msg);
            }
        };

        println!("Interval: {}s", tracker_resp.interval);

        fn print_opt_field<T: Display>(x: Option<T>, name: &str) {
            if let Some(x) = x {
                println!("{}: {}", name, x);
            }
        }

        print_opt_field(tracker_resp.warning_message, "Warning");
        print_opt_field(tracker_resp.min_interval, "Min. interval");
        print_opt_field(tracker_resp.tracker_id, "Tracker ID");
        print_opt_field(tracker_resp.complete, "Complete");
        print_opt_field(tracker_resp.incomplete, "Incomplete");

        match tracker_resp.peers {
            TrackerResponsePeerList::Compact(peers) => {
                println!("Peers: {}; compact", peers.len());
                for p in peers {
                    println!("{}", p.addr);
                }
            }
            TrackerResponsePeerList::Normal(peers) => {
                println!("Peers: {}; normal", peers.len());
                for p in peers {
                    println!("{}:{}", p.ip, p.port);
                }
            }
        }
    } else {
        query_udp_tracker(&announce_url, &request_base, &info_hash);
    }
}
